﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstructionsController
struct  InstructionsController_t1323375089  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject InstructionsController::jumpInstructions
	GameObject_t1756533147 * ___jumpInstructions_2;
	// UnityEngine.GameObject InstructionsController::powerInstructions
	GameObject_t1756533147 * ___powerInstructions_3;
	// UnityEngine.GameObject InstructionsController::capeInstructions
	GameObject_t1756533147 * ___capeInstructions_4;
	// UnityEngine.GameObject InstructionsController::eyeInstructions
	GameObject_t1756533147 * ___eyeInstructions_5;
	// UnityEngine.GameObject InstructionsController::wallInstructions
	GameObject_t1756533147 * ___wallInstructions_6;
	// System.Boolean InstructionsController::timerRunning
	bool ___timerRunning_7;
	// System.Single InstructionsController::timer
	float ___timer_8;

public:
	inline static int32_t get_offset_of_jumpInstructions_2() { return static_cast<int32_t>(offsetof(InstructionsController_t1323375089, ___jumpInstructions_2)); }
	inline GameObject_t1756533147 * get_jumpInstructions_2() const { return ___jumpInstructions_2; }
	inline GameObject_t1756533147 ** get_address_of_jumpInstructions_2() { return &___jumpInstructions_2; }
	inline void set_jumpInstructions_2(GameObject_t1756533147 * value)
	{
		___jumpInstructions_2 = value;
		Il2CppCodeGenWriteBarrier(&___jumpInstructions_2, value);
	}

	inline static int32_t get_offset_of_powerInstructions_3() { return static_cast<int32_t>(offsetof(InstructionsController_t1323375089, ___powerInstructions_3)); }
	inline GameObject_t1756533147 * get_powerInstructions_3() const { return ___powerInstructions_3; }
	inline GameObject_t1756533147 ** get_address_of_powerInstructions_3() { return &___powerInstructions_3; }
	inline void set_powerInstructions_3(GameObject_t1756533147 * value)
	{
		___powerInstructions_3 = value;
		Il2CppCodeGenWriteBarrier(&___powerInstructions_3, value);
	}

	inline static int32_t get_offset_of_capeInstructions_4() { return static_cast<int32_t>(offsetof(InstructionsController_t1323375089, ___capeInstructions_4)); }
	inline GameObject_t1756533147 * get_capeInstructions_4() const { return ___capeInstructions_4; }
	inline GameObject_t1756533147 ** get_address_of_capeInstructions_4() { return &___capeInstructions_4; }
	inline void set_capeInstructions_4(GameObject_t1756533147 * value)
	{
		___capeInstructions_4 = value;
		Il2CppCodeGenWriteBarrier(&___capeInstructions_4, value);
	}

	inline static int32_t get_offset_of_eyeInstructions_5() { return static_cast<int32_t>(offsetof(InstructionsController_t1323375089, ___eyeInstructions_5)); }
	inline GameObject_t1756533147 * get_eyeInstructions_5() const { return ___eyeInstructions_5; }
	inline GameObject_t1756533147 ** get_address_of_eyeInstructions_5() { return &___eyeInstructions_5; }
	inline void set_eyeInstructions_5(GameObject_t1756533147 * value)
	{
		___eyeInstructions_5 = value;
		Il2CppCodeGenWriteBarrier(&___eyeInstructions_5, value);
	}

	inline static int32_t get_offset_of_wallInstructions_6() { return static_cast<int32_t>(offsetof(InstructionsController_t1323375089, ___wallInstructions_6)); }
	inline GameObject_t1756533147 * get_wallInstructions_6() const { return ___wallInstructions_6; }
	inline GameObject_t1756533147 ** get_address_of_wallInstructions_6() { return &___wallInstructions_6; }
	inline void set_wallInstructions_6(GameObject_t1756533147 * value)
	{
		___wallInstructions_6 = value;
		Il2CppCodeGenWriteBarrier(&___wallInstructions_6, value);
	}

	inline static int32_t get_offset_of_timerRunning_7() { return static_cast<int32_t>(offsetof(InstructionsController_t1323375089, ___timerRunning_7)); }
	inline bool get_timerRunning_7() const { return ___timerRunning_7; }
	inline bool* get_address_of_timerRunning_7() { return &___timerRunning_7; }
	inline void set_timerRunning_7(bool value)
	{
		___timerRunning_7 = value;
	}

	inline static int32_t get_offset_of_timer_8() { return static_cast<int32_t>(offsetof(InstructionsController_t1323375089, ___timer_8)); }
	inline float get_timer_8() const { return ___timer_8; }
	inline float* get_address_of_timer_8() { return &___timer_8; }
	inline void set_timer_8(float value)
	{
		___timer_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
