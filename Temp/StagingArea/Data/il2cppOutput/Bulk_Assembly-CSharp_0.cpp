﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_CapePickup3725470329.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharp_HeroController4041626604.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_EyePickup2220146595.h"
#include "AssemblyU2DCSharp_FinishLevel1405972627.h"
#include "AssemblyU2DCSharp_LevelGenerator1121608959.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_FloorController3203059022.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "AssemblyU2DCSharp_GameController3607102586.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_LaserController2836074815.h"
#include "UnityEngine_UnityEngine_LineRenderer849157671.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_InstructionsController1323375089.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "AssemblyU2DCSharp_LaserController_U3CShowLaserU3Ec1127661705.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_MenuController848154101.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "AssemblyU2DCSharp_Mover873752897.h"
#include "AssemblyU2DCSharp_ObstacleController2319665031.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "AssemblyU2DCSharp_PowerupController1946057392.h"

// CapePickup
struct CapePickup_t3725470329;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Component
struct Component_t3819376471;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// HeroController
struct HeroController_t4041626604;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Object
struct Object_t1021602117;
// EyePickup
struct EyePickup_t2220146595;
// FinishLevel
struct FinishLevel_t1405972627;
// FloorController
struct FloorController_t3203059022;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// GameController
struct GameController_t3607102586;
// UnityEngine.Camera
struct Camera_t189460977;
// LaserController
struct LaserController_t2836074815;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// InstructionsController
struct InstructionsController_t1323375089;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// LaserController/<ShowLaser>c__Iterator0
struct U3CShowLaserU3Ec__Iterator0_t1127661705;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// LevelGenerator
struct LevelGenerator_t1121608959;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// MenuController
struct MenuController_t848154101;
// UnityEngine.UI.Image
struct Image_t2042527209;
// Mover
struct Mover_t873752897;
// ObstacleController
struct ObstacleController_t2319665031;
// PowerupController
struct PowerupController_t1946057392;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisHeroController_t4041626604_m2559195979_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t CapePickup_OnTriggerEnter2D_m1299857662_MetadataUsageId;
extern const uint32_t EyePickup_OnTriggerEnter2D_m2790909292_MetadataUsageId;
extern Il2CppClass* LevelGenerator_t1121608959_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral120639914;
extern Il2CppCodeGenString* _stringLiteral694878703;
extern const uint32_t FinishLevel_OnTriggerEnter2D_m357665654_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisHeroController_t4041626604_m1708674487_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4183277322;
extern const uint32_t FloorController_OnCollisionEnter2D_m4042294953_MetadataUsageId;
extern const uint32_t GameController_Start_m239487205_MetadataUsageId;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1543969241;
extern Il2CppCodeGenString* _stringLiteral372029387;
extern const uint32_t GameController_Update_m1556003900_MetadataUsageId;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisLaserController_t2836074815_m3326908270_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisLineRenderer_t849157671_m3748809323_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3196376945;
extern const uint32_t HeroController_Start_m3180304363_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var;
extern const uint32_t HeroController_Jump_m1953585121_MetadataUsageId;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2123790461;
extern Il2CppCodeGenString* _stringLiteral4077353881;
extern const uint32_t HeroController_ShootAt_m4026011997_MetadataUsageId;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2002042674;
extern const uint32_t HeroController_ReplenishPower_m2136775080_MetadataUsageId;
extern Il2CppClass* U3CShowLaserU3Ec__Iterator0_t1127661705_il2cpp_TypeInfo_var;
extern const uint32_t LaserController_ShowLaser_m129560462_MetadataUsageId;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CShowLaserU3Ec__Iterator0_MoveNext_m199517420_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CShowLaserU3Ec__Iterator0_Reset_m2880137833_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3015339463;
extern const uint32_t LevelGenerator_Start_m4240987518_MetadataUsageId;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m351711267_MethodInfo_var;
extern const uint32_t LevelGenerator_GenerateTile_m2057031207_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3912186174;
extern Il2CppCodeGenString* _stringLiteral3335872661;
extern Il2CppCodeGenString* _stringLiteral549601424;
extern const uint32_t MenuController_Start_m3141389506_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral24946573;
extern Il2CppCodeGenString* _stringLiteral2328219732;
extern const uint32_t MenuController_StartLevel_m1694854699_MetadataUsageId;
extern const uint32_t ObstacleController_OnCollisionEnter2D_m1129342162_MetadataUsageId;
extern const uint32_t PowerupController_OnTriggerEnter2D_m1219282109_MetadataUsageId;

// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2724090252  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Texture2D_t3542995729 * m_Items[1];

public:
	inline Texture2D_t3542995729 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Texture2D_t3542995729 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Texture2D_t3542995729 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Texture2D_t3542995729 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Texture2D_t3542995729 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Texture2D_t3542995729 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m360069213_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, Transform_t3275118058 * p3, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Component::CompareTag(System.String)
extern "C"  bool Component_CompareTag_m3443292365 (Component_t3819376471 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3275118058 * Transform_get_parent_m147407266 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<HeroController>()
#define Component_GetComponent_TisHeroController_t4041626604_m2559195979(__this, method) ((  HeroController_t4041626604 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void HeroController::PickupCape()
extern "C"  void HeroController_PickupCape_m3808310600 (HeroController_t4041626604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::PickupEye()
extern "C"  void HeroController_PickupEye_m3822236954 (HeroController_t4041626604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m136681260 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m3351928596 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m1619949821 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Collision2D::get_gameObject()
extern "C"  GameObject_t1756533147 * Collision2D_get_gameObject_m4234358314 (Collision2D_t1539500754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::CompareTag(System.String)
extern "C"  bool GameObject_CompareTag_m2797152613 (GameObject_t1756533147 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m836511350 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<HeroController>()
#define GameObject_GetComponent_TisHeroController_t4041626604_m1708674487(__this, method) ((  HeroController_t4041626604 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void HeroController::LandOnFloor()
extern "C"  void HeroController_LandOnFloor_m387675429 (HeroController_t4041626604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t407273883  Input_GetTouch_m1463942798 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m196706494 (Touch_t407273883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2243707579  Touch_get_position_m2079703643 (Touch_t407273883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::HandleTouchAt(UnityEngine.Vector2)
extern "C"  void GameController_HandleTouchAt_m4127432543 (GameController_t3607102586 * __this, Vector2_t2243707579  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m2050827666 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m47917805 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t2243707580  Input_get_mousePosition_m146923508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Vector2_op_Implicit_m1064335535 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(System.String)
extern "C"  bool Input_GetKeyDown_m1749539436 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::AttemptJump()
extern "C"  void HeroController_AttemptJump_m895324234 (HeroController_t4041626604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m41137238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 GameController::WorldPoint(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  GameController_WorldPoint_m3007667404 (GameController_t3607102586 * __this, Vector2_t2243707579  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::AttemptShotAt(UnityEngine.Vector2)
extern "C"  void HeroController_AttemptShotAt_m676128567 (HeroController_t4041626604 * __this, Vector2_t2243707579  ___targetPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m475173995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_ScreenToWorldPoint_m929392728 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<LaserController>()
#define GameObject_GetComponent_TisLaserController_t2836074815_m3326908270(__this, method) ((  LaserController_t2836074815 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.LineRenderer>()
#define GameObject_GetComponent_TisLineRenderer_t849157671_m3748809323(__this, method) ((  LineRenderer_t849157671 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::UpdatePowerMeter()
extern "C"  void HeroController_UpdatePowerMeter_m1796648232 (HeroController_t4041626604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::Jump()
extern "C"  void HeroController_Jump_m1953585121 (HeroController_t4041626604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody2D>()
#define GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(__this, method) ((  Rigidbody2D_t502193897 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C"  Vector2_t2243707579  Rigidbody2D_get_velocity_m3310151195 (Rigidbody2D_t502193897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m3592751374 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::ShootAt(UnityEngine.Vector2)
extern "C"  void HeroController_ShootAt_m4026011997 (HeroController_t4041626604 * __this, Vector2_t2243707579  ___targetPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Subtraction_m1984215297 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  p0, Vector2_t2243707579  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C"  float Vector2_get_magnitude_m33802565 (Vector2_t2243707579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m3913913442 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  p0, Vector2_t2243707579  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RaycastHit2D::op_Implicit(UnityEngine.RaycastHit2D)
extern "C"  bool RaycastHit2D_op_Implicit_m596912073 (Il2CppObject * __this /* static, unused */, RaycastHit2D_t4063908774  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t646061738 * RaycastHit2D_get_collider_m2568504212 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2243707580  Vector2_op_Implicit_m176791411 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetPosition(System.Int32,UnityEngine.Vector3)
extern "C"  void LineRenderer_SetPosition_m4048451705 (LineRenderer_t849157671 * __this, int32_t p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaserController::ShowLaser()
extern "C"  Il2CppObject * LaserController_ShowLaser_m129560462 (LaserController_t2836074815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1648492575 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionsController::ShowPowerInstructions()
extern "C"  void InstructionsController_ShowPowerInstructions_m1224693151 (InstructionsController_t1323375089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionsController::ShowCapeInstructions()
extern "C"  void InstructionsController_ShowCapeInstructions_m3720995727 (InstructionsController_t1323375089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2887581199 (GameObject_t1756533147 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionsController::ShowEyeInstructions()
extern "C"  void InstructionsController_ShowEyeInstructions_m2288408641 (InstructionsController_t1323375089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m2319668137 (RectTransform_t3349966182 * __this, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionsController::HideAll()
extern "C"  void InstructionsController_HideAll_m3022444469 (InstructionsController_t1323375089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionsController::StartTimer()
extern "C"  void InstructionsController_StartTimer_m2090827987 (InstructionsController_t1323375089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaserController/<ShowLaser>c__Iterator0::.ctor()
extern "C"  void U3CShowLaserU3Ec__Iterator0__ctor_m591712404 (U3CShowLaserU3Ec__Iterator0_t1127661705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m1990515539 (WaitForSeconds_t3839502067 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaserController::HideLaser()
extern "C"  void LaserController_HideLaser_m4253751403 (LaserController_t2836074815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3811852957 (Color_t2020392075 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelGenerator::GenerateLevel(System.Int32)
extern "C"  void LevelGenerator_GenerateLevel_m1877206344 (LevelGenerator_t1121608959 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionsController::ShowWallInstructions()
extern "C"  void InstructionsController_ShowWallInstructions_m683673258 (InstructionsController_t1323375089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InstructionsController::ShowJumpInstructions()
extern "C"  void InstructionsController_ShowJumpInstructions_m34564830 (InstructionsController_t1323375089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Texture2D::GetPixel(System.Int32,System.Int32)
extern "C"  Color_t2020392075  Texture2D_GetPixel_m1570987051 (Texture2D_t3542995729 * __this, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelGenerator::GenerateTile(System.Int32,System.Int32,UnityEngine.Color)
extern "C"  void LevelGenerator_GenerateTile_m2057031207 (LevelGenerator_t1121608959 * __this, int32_t ___x0, int32_t ___y1, Color_t2020392075  ___pixelColor2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m661618137 (Color_t2020392075 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m1561886418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
#define Object_Instantiate_TisGameObject_t1756533147_m351711267(__this /* static, unused */, p0, p1, p2, p3, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m360069213_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m1033555130 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ColorUtility::TryParseHtmlString(System.String,UnityEngine.Color&)
extern "C"  bool ColorUtility_TryParseHtmlString_m3515386476 (Il2CppObject * __this /* static, unused */, String_t* p0, Color_t2020392075 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2042527209_m4162535761(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(System.Single,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Multiply_m3393065202 (Il2CppObject * __this /* static, unused */, float p0, Vector2_t2243707579  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t2243707580  Transform_get_localPosition_m2533925116 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m1026930133 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1684909666  SceneManager_GetActiveScene_m2964039490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m745914591 (Scene_t1684909666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroController::ReplenishPower()
extern "C"  void HeroController_ReplenishPower_m2136775080 (HeroController_t4041626604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CapePickup::.ctor()
extern "C"  void CapePickup__ctor_m4217395110 (CapePickup_t3725470329 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CapePickup::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void CapePickup_OnTriggerEnter2D_m1299857662 (CapePickup_t3725470329 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CapePickup_OnTriggerEnter2D_m1299857662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HeroController_t4041626604 * V_0 = NULL;
	{
		Collider2D_t646061738 * L_0 = ___other0;
		bool L_1 = Component_CompareTag_m3443292365(L_0, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		Collider2D_t646061738 * L_2 = ___other0;
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Transform_get_parent_m147407266(L_3, /*hidden argument*/NULL);
		HeroController_t4041626604 * L_5 = Component_GetComponent_TisHeroController_t4041626604_m2559195979(L_4, /*hidden argument*/Component_GetComponent_TisHeroController_t4041626604_m2559195979_MethodInfo_var);
		V_0 = L_5;
		HeroController_t4041626604 * L_6 = V_0;
		HeroController_PickupCape_m3808310600(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void EyePickup::.ctor()
extern "C"  void EyePickup__ctor_m3126077488 (EyePickup_t2220146595 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EyePickup::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void EyePickup_OnTriggerEnter2D_m2790909292 (EyePickup_t2220146595 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EyePickup_OnTriggerEnter2D_m2790909292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HeroController_t4041626604 * V_0 = NULL;
	{
		Collider2D_t646061738 * L_0 = ___other0;
		bool L_1 = Component_CompareTag_m3443292365(L_0, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		Collider2D_t646061738 * L_2 = ___other0;
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Transform_get_parent_m147407266(L_3, /*hidden argument*/NULL);
		HeroController_t4041626604 * L_5 = Component_GetComponent_TisHeroController_t4041626604_m2559195979(L_4, /*hidden argument*/Component_GetComponent_TisHeroController_t4041626604_m2559195979_MethodInfo_var);
		V_0 = L_5;
		HeroController_t4041626604 * L_6 = V_0;
		HeroController_PickupEye_m3822236954(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void FinishLevel::.ctor()
extern "C"  void FinishLevel__ctor_m1669453954 (FinishLevel_t1405972627 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FinishLevel::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void FinishLevel_OnTriggerEnter2D_m357665654 (FinishLevel_t1405972627 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FinishLevel_OnTriggerEnter2D_m357665654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___other0;
		bool L_1 = Component_CompareTag_m3443292365(L_0, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_2 = ((LevelGenerator_t1121608959_StaticFields*)LevelGenerator_t1121608959_il2cpp_TypeInfo_var->static_fields)->get_level_18();
		int32_t L_3 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral120639914, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)L_3)))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_4 = ((LevelGenerator_t1121608959_StaticFields*)LevelGenerator_t1121608959_il2cpp_TypeInfo_var->static_fields)->get_level_18();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral120639914, L_4, /*hidden argument*/NULL);
	}

IL_0034:
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral694878703, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void FloorController::.ctor()
extern "C"  void FloorController__ctor_m3120668591 (FloorController_t3203059022 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FloorController::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void FloorController_OnCollisionEnter2D_m4042294953 (FloorController_t3203059022 * __this, Collision2D_t1539500754 * ___collision0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FloorController_OnCollisionEnter2D_m4042294953_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HeroController_t4041626604 * V_0 = NULL;
	{
		Collision2D_t1539500754 * L_0 = ___collision0;
		GameObject_t1756533147 * L_1 = Collision2D_get_gameObject_m4234358314(L_0, /*hidden argument*/NULL);
		bool L_2 = GameObject_CompareTag_m2797152613(L_1, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		GameObject_t1756533147 * L_3 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral4183277322, /*hidden argument*/NULL);
		HeroController_t4041626604 * L_4 = GameObject_GetComponent_TisHeroController_t4041626604_m1708674487(L_3, /*hidden argument*/GameObject_GetComponent_TisHeroController_t4041626604_m1708674487_MethodInfo_var);
		V_0 = L_4;
		HeroController_t4041626604 * L_5 = V_0;
		HeroController_LandOnFloor_m387675429(L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m1439649957 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Start()
extern "C"  void GameController_Start_m239487205 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Start_m239487205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_heroContainer_3();
		HeroController_t4041626604 * L_1 = GameObject_GetComponent_TisHeroController_t4041626604_m1708674487(L_0, /*hidden argument*/GameObject_GetComponent_TisHeroController_t4041626604_m1708674487_MethodInfo_var);
		__this->set_heroController_4(L_1);
		return;
	}
}
// System.Void GameController::Update()
extern "C"  void GameController_Update_m1556003900 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Update_m1556003900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Touch_t407273883  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_isMobile_2();
		if (!L_0)
		{
			goto IL_0046;
		}
	}
	{
		V_0 = 0;
		goto IL_0036;
	}

IL_0012:
	{
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_2 = Input_GetTouch_m1463942798(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Touch_get_phase_m196706494((&V_1), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		Vector2_t2243707579  L_4 = Touch_get_position_m2079703643((&V_1), /*hidden argument*/NULL);
		GameController_HandleTouchAt_m4127432543(__this, L_4, /*hidden argument*/NULL);
	}

IL_0032:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_7 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0012;
		}
	}
	{
		goto IL_0061;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_9 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		GameController_HandleTouchAt_m4127432543(__this, L_10, /*hidden argument*/NULL);
	}

IL_0061:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_11 = Input_GetKeyDown_m1749539436(NULL /*static, unused*/, _stringLiteral1543969241, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_007f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetKeyDown_m1749539436(NULL /*static, unused*/, _stringLiteral372029387, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_008a;
		}
	}

IL_007f:
	{
		HeroController_t4041626604 * L_13 = __this->get_heroController_4();
		HeroController_AttemptJump_m895324234(L_13, /*hidden argument*/NULL);
	}

IL_008a:
	{
		return;
	}
}
// System.Void GameController::HandleTouchAt(UnityEngine.Vector2)
extern "C"  void GameController_HandleTouchAt_m4127432543 (GameController_t3607102586 * __this, Vector2_t2243707579  ___pos0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isMobile_2();
		if (!L_0)
		{
			goto IL_0046;
		}
	}
	{
		float L_1 = (&___pos0)->get_x_0();
		int32_t L_2 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(((float)((float)((int32_t)((int32_t)L_2/(int32_t)5)))))))))
		{
			goto IL_0036;
		}
	}
	{
		HeroController_t4041626604 * L_3 = __this->get_heroController_4();
		Vector2_t2243707579  L_4 = ___pos0;
		Vector2_t2243707579  L_5 = GameController_WorldPoint_m3007667404(__this, L_4, /*hidden argument*/NULL);
		HeroController_AttemptShotAt_m676128567(L_3, L_5, /*hidden argument*/NULL);
		goto IL_0041;
	}

IL_0036:
	{
		HeroController_t4041626604 * L_6 = __this->get_heroController_4();
		HeroController_AttemptJump_m895324234(L_6, /*hidden argument*/NULL);
	}

IL_0041:
	{
		goto IL_0058;
	}

IL_0046:
	{
		HeroController_t4041626604 * L_7 = __this->get_heroController_4();
		Vector2_t2243707579  L_8 = ___pos0;
		Vector2_t2243707579  L_9 = GameController_WorldPoint_m3007667404(__this, L_8, /*hidden argument*/NULL);
		HeroController_AttemptShotAt_m676128567(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// UnityEngine.Vector2 GameController::WorldPoint(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  GameController_WorldPoint_m3007667404 (GameController_t3607102586 * __this, Vector2_t2243707579  ___pos0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = (&___pos0)->get_x_0();
		float L_1 = (&___pos0)->get_y_1();
		Vector3__ctor_m2638739322((&V_0), L_0, L_1, (0.0f), /*hidden argument*/NULL);
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = V_0;
		Vector3_t2243707580  L_4 = Camera_ScreenToWorldPoint_m929392728(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_x_1();
		float L_6 = (&V_1)->get_y_2();
		Vector2_t2243707579  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2__ctor_m3067419446(&L_7, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void HeroController::.ctor()
extern "C"  void HeroController__ctor_m3530452135 (HeroController_t4041626604 * __this, const MethodInfo* method)
{
	{
		__this->set_maxPower_6((100.0f));
		__this->set_powerToJump_7((5.0f));
		__this->set_powerToShoot_8((15.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HeroController::Start()
extern "C"  void HeroController_Start_m3180304363 (HeroController_t4041626604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController_Start_m3180304363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_powerRemaining_9((0.0f));
		GameObject_t1756533147 * L_0 = __this->get_laser_14();
		LaserController_t2836074815 * L_1 = GameObject_GetComponent_TisLaserController_t2836074815_m3326908270(L_0, /*hidden argument*/GameObject_GetComponent_TisLaserController_t2836074815_m3326908270_MethodInfo_var);
		__this->set_laserController_16(L_1);
		GameObject_t1756533147 * L_2 = __this->get_laser_14();
		LineRenderer_t849157671 * L_3 = GameObject_GetComponent_TisLineRenderer_t849157671_m3748809323(L_2, /*hidden argument*/GameObject_GetComponent_TisLineRenderer_t849157671_m3748809323_MethodInfo_var);
		__this->set_laserRenderer_15(L_3);
		__this->set_isOnFloor_13((bool)0);
		int32_t L_4 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral3196376945, 1, /*hidden argument*/NULL);
		__this->set_hasFlight_10((bool)((((int32_t)L_4) > ((int32_t)1))? 1 : 0));
		int32_t L_5 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral3196376945, 1, /*hidden argument*/NULL);
		__this->set_hasLaserVision_11((bool)((((int32_t)L_5) > ((int32_t)1))? 1 : 0));
		bool L_6 = __this->get_hasFlight_10();
		bool L_7 = L_6;
		Il2CppObject * L_8 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		HeroController_UpdatePowerMeter_m1796648232(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HeroController::AttemptJump()
extern "C"  void HeroController_AttemptJump_m895324234 (HeroController_t4041626604 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isOnFloor_13();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		HeroController_Jump_m1953585121(__this, /*hidden argument*/NULL);
		goto IL_0051;
	}

IL_0016:
	{
		bool L_1 = __this->get_hasFlight_10();
		if (!L_1)
		{
			goto IL_0051;
		}
	}
	{
		float L_2 = __this->get_powerRemaining_9();
		float L_3 = __this->get_powerToJump_7();
		if ((!(((float)L_2) >= ((float)L_3))))
		{
			goto IL_0051;
		}
	}
	{
		HeroController_Jump_m1953585121(__this, /*hidden argument*/NULL);
		float L_4 = __this->get_powerRemaining_9();
		float L_5 = __this->get_powerToJump_7();
		__this->set_powerRemaining_9(((float)((float)L_4-(float)L_5)));
		HeroController_UpdatePowerMeter_m1796648232(__this, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void HeroController::Jump()
extern "C"  void HeroController_Jump_m1953585121 (HeroController_t4041626604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController_Jump_m1953585121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_t502193897 * V_0 = NULL;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObject_t1756533147 * L_0 = __this->get_body_3();
		Rigidbody2D_t502193897 * L_1 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_0, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		V_0 = L_1;
		Rigidbody2D_t502193897 * L_2 = V_0;
		Vector2_t2243707579  L_3 = Rigidbody2D_get_velocity_m3310151195(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = __this->get_jumpPower_2();
		(&V_1)->set_y_1(L_4);
		Rigidbody2D_t502193897 * L_5 = V_0;
		Vector2_t2243707579  L_6 = V_1;
		Rigidbody2D_set_velocity_m3592751374(L_5, L_6, /*hidden argument*/NULL);
		__this->set_isOnFloor_13((bool)0);
		return;
	}
}
// System.Void HeroController::LandOnFloor()
extern "C"  void HeroController_LandOnFloor_m387675429 (HeroController_t4041626604 * __this, const MethodInfo* method)
{
	{
		__this->set_isOnFloor_13((bool)1);
		return;
	}
}
// System.Void HeroController::AttemptShotAt(UnityEngine.Vector2)
extern "C"  void HeroController_AttemptShotAt_m676128567 (HeroController_t4041626604 * __this, Vector2_t2243707579  ___targetPos0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_hasLaserVision_11();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		float L_1 = __this->get_powerRemaining_9();
		float L_2 = __this->get_powerToShoot_8();
		if ((!(((float)L_1) >= ((float)L_2))))
		{
			goto IL_0023;
		}
	}
	{
		Vector2_t2243707579  L_3 = ___targetPos0;
		HeroController_ShootAt_m4026011997(__this, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void HeroController::ShootAt(UnityEngine.Vector2)
extern "C"  void HeroController_ShootAt_m4026011997 (HeroController_t4041626604 * __this, Vector2_t2243707579  ___targetPos0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController_ShootAt_m4026011997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	RaycastHit2D_t4063908774  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		GameObject_t1756533147 * L_0 = __this->get_eye_4();
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2243707579  L_4 = ___targetPos0;
		Vector2_t2243707579  L_5 = V_0;
		Vector2_t2243707579  L_6 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = Vector2_get_magnitude_m33802565((&V_1), /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = V_2;
		float L_9 = L_8;
		Il2CppObject * L_10 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = V_0;
		Vector2_t2243707579  L_12 = V_1;
		float L_13 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_14 = Physics2D_Raycast_m3913913442(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		RaycastHit2D_t4063908774  L_15 = V_3;
		bool L_16 = RaycastHit2D_op_Implicit_m596912073(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b3;
		}
	}
	{
		Collider2D_t646061738 * L_17 = RaycastHit2D_get_collider_m2568504212((&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00b3;
		}
	}
	{
		Collider2D_t646061738 * L_19 = RaycastHit2D_get_collider_m2568504212((&V_3), /*hidden argument*/NULL);
		bool L_20 = Component_CompareTag_m3443292365(L_19, _stringLiteral2123790461, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0082;
		}
	}
	{
		Collider2D_t646061738 * L_21 = RaycastHit2D_get_collider_m2568504212((&V_3), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_22 = Component_get_gameObject_m3105766835(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_0082:
	{
		Collider2D_t646061738 * L_23 = RaycastHit2D_get_collider_m2568504212((&V_3), /*hidden argument*/NULL);
		bool L_24 = Component_CompareTag_m3443292365(L_23, _stringLiteral4077353881, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b3;
		}
	}
	{
		Collider2D_t646061738 * L_25 = RaycastHit2D_get_collider_m2568504212((&V_3), /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = Component_get_transform_m2697483695(L_25, /*hidden argument*/NULL);
		Transform_t3275118058 * L_27 = Transform_get_parent_m147407266(L_26, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_28 = Component_get_gameObject_m3105766835(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		LineRenderer_t849157671 * L_29 = __this->get_laserRenderer_15();
		Vector2_t2243707579  L_30 = V_0;
		Vector3_t2243707580  L_31 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		LineRenderer_SetPosition_m4048451705(L_29, 0, L_31, /*hidden argument*/NULL);
		LineRenderer_t849157671 * L_32 = __this->get_laserRenderer_15();
		Vector2_t2243707579  L_33 = ___targetPos0;
		Vector3_t2243707580  L_34 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		LineRenderer_SetPosition_m4048451705(L_32, 1, L_34, /*hidden argument*/NULL);
		LaserController_t2836074815 * L_35 = __this->get_laserController_16();
		Il2CppObject * L_36 = LaserController_ShowLaser_m129560462(L_35, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_36, /*hidden argument*/NULL);
		float L_37 = __this->get_powerRemaining_9();
		float L_38 = __this->get_powerToShoot_8();
		__this->set_powerRemaining_9(((float)((float)L_37-(float)L_38)));
		HeroController_UpdatePowerMeter_m1796648232(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HeroController::ReplenishPower()
extern "C"  void HeroController_ReplenishPower_m2136775080 (HeroController_t4041626604 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HeroController_ReplenishPower_m2136775080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		float L_0 = __this->get_powerRemaining_9();
		float L_1 = __this->get_maxPower_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m1648492575(NULL /*static, unused*/, ((float)((float)L_0+(float)(50.0f))), L_1, /*hidden argument*/NULL);
		__this->set_powerRemaining_9(L_2);
		HeroController_UpdatePowerMeter_m1796648232(__this, /*hidden argument*/NULL);
		int32_t L_3 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral2002042674, 0, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_5 = ((LevelGenerator_t1121608959_StaticFields*)LevelGenerator_t1121608959_il2cpp_TypeInfo_var->static_fields)->get_level_18();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_004e;
		}
	}
	{
		InstructionsController_t1323375089 * L_6 = __this->get_instructions_17();
		InstructionsController_ShowPowerInstructions_m1224693151(L_6, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void HeroController::PickupCape()
extern "C"  void HeroController_PickupCape_m3808310600 (HeroController_t4041626604 * __this, const MethodInfo* method)
{
	{
		__this->set_hasFlight_10((bool)1);
		InstructionsController_t1323375089 * L_0 = __this->get_instructions_17();
		InstructionsController_ShowCapeInstructions_m3720995727(L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_cape_5();
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HeroController::PickupEye()
extern "C"  void HeroController_PickupEye_m3822236954 (HeroController_t4041626604 * __this, const MethodInfo* method)
{
	{
		__this->set_hasLaserVision_11((bool)1);
		InstructionsController_t1323375089 * L_0 = __this->get_instructions_17();
		InstructionsController_ShowEyeInstructions_m2288408641(L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_eye_4();
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HeroController::UpdatePowerMeter()
extern "C"  void HeroController_UpdatePowerMeter_m1796648232 (HeroController_t4041626604 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_powerRemaining_9();
		float L_1 = __this->get_maxPower_6();
		V_0 = ((float)((float)((float)((float)(100.0f)*(float)L_0))/(float)L_1));
		RectTransform_t3349966182 * L_2 = __this->get_powerMeter_12();
		float L_3 = V_0;
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, L_3, (15.0f), /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m2319668137(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InstructionsController::.ctor()
extern "C"  void InstructionsController__ctor_m578098128 (InstructionsController_t1323375089 * __this, const MethodInfo* method)
{
	{
		__this->set_timer_8((4.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InstructionsController::Start()
extern "C"  void InstructionsController_Start_m2021990448 (InstructionsController_t1323375089 * __this, const MethodInfo* method)
{
	{
		InstructionsController_HideAll_m3022444469(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InstructionsController::Update()
extern "C"  void InstructionsController_Update_m712506585 (InstructionsController_t1323375089 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_timerRunning_7();
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		float L_1 = __this->get_timer_8();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_8(((float)((float)L_1-(float)L_2)));
		float L_3 = __this->get_timer_8();
		if ((!(((float)L_3) <= ((float)(0.0f)))))
		{
			goto IL_003a;
		}
	}
	{
		InstructionsController_HideAll_m3022444469(__this, /*hidden argument*/NULL);
		__this->set_timerRunning_7((bool)0);
	}

IL_003a:
	{
		return;
	}
}
// System.Void InstructionsController::StartTimer()
extern "C"  void InstructionsController_StartTimer_m2090827987 (InstructionsController_t1323375089 * __this, const MethodInfo* method)
{
	{
		__this->set_timer_8((4.0f));
		__this->set_timerRunning_7((bool)1);
		return;
	}
}
// System.Void InstructionsController::HideAll()
extern "C"  void InstructionsController_HideAll_m3022444469 (InstructionsController_t1323375089 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_jumpInstructions_2();
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_wallInstructions_6();
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_eyeInstructions_5();
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_capeInstructions_4();
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_powerInstructions_3();
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InstructionsController::ShowJumpInstructions()
extern "C"  void InstructionsController_ShowJumpInstructions_m34564830 (InstructionsController_t1323375089 * __this, const MethodInfo* method)
{
	{
		InstructionsController_HideAll_m3022444469(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = __this->get_jumpInstructions_2();
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		InstructionsController_StartTimer_m2090827987(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InstructionsController::ShowWallInstructions()
extern "C"  void InstructionsController_ShowWallInstructions_m683673258 (InstructionsController_t1323375089 * __this, const MethodInfo* method)
{
	{
		InstructionsController_HideAll_m3022444469(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = __this->get_wallInstructions_6();
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		InstructionsController_StartTimer_m2090827987(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InstructionsController::ShowCapeInstructions()
extern "C"  void InstructionsController_ShowCapeInstructions_m3720995727 (InstructionsController_t1323375089 * __this, const MethodInfo* method)
{
	{
		InstructionsController_HideAll_m3022444469(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = __this->get_capeInstructions_4();
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		InstructionsController_StartTimer_m2090827987(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InstructionsController::ShowEyeInstructions()
extern "C"  void InstructionsController_ShowEyeInstructions_m2288408641 (InstructionsController_t1323375089 * __this, const MethodInfo* method)
{
	{
		InstructionsController_HideAll_m3022444469(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = __this->get_eyeInstructions_5();
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		InstructionsController_StartTimer_m2090827987(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InstructionsController::ShowPowerInstructions()
extern "C"  void InstructionsController_ShowPowerInstructions_m1224693151 (InstructionsController_t1323375089 * __this, const MethodInfo* method)
{
	{
		InstructionsController_HideAll_m3022444469(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = __this->get_powerInstructions_3();
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		InstructionsController_StartTimer_m2090827987(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LaserController::.ctor()
extern "C"  void LaserController__ctor_m645837514 (LaserController_t2836074815 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator LaserController::ShowLaser()
extern "C"  Il2CppObject * LaserController_ShowLaser_m129560462 (LaserController_t2836074815 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LaserController_ShowLaser_m129560462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CShowLaserU3Ec__Iterator0_t1127661705 * V_0 = NULL;
	{
		U3CShowLaserU3Ec__Iterator0_t1127661705 * L_0 = (U3CShowLaserU3Ec__Iterator0_t1127661705 *)il2cpp_codegen_object_new(U3CShowLaserU3Ec__Iterator0_t1127661705_il2cpp_TypeInfo_var);
		U3CShowLaserU3Ec__Iterator0__ctor_m591712404(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowLaserU3Ec__Iterator0_t1127661705 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CShowLaserU3Ec__Iterator0_t1127661705 * L_2 = V_0;
		return L_2;
	}
}
// System.Void LaserController::HideLaser()
extern "C"  void LaserController_HideLaser_m4253751403 (LaserController_t2836074815 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LaserController/<ShowLaser>c__Iterator0::.ctor()
extern "C"  void U3CShowLaserU3Ec__Iterator0__ctor_m591712404 (U3CShowLaserU3Ec__Iterator0_t1127661705 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LaserController/<ShowLaser>c__Iterator0::MoveNext()
extern "C"  bool U3CShowLaserU3Ec__Iterator0_MoveNext_m199517420 (U3CShowLaserU3Ec__Iterator0_t1127661705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowLaserU3Ec__Iterator0_MoveNext_m199517420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0056;
		}
	}
	{
		goto IL_0068;
	}

IL_0021:
	{
		LaserController_t2836074815 * L_2 = __this->get_U24this_0();
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, (0.05f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		bool L_5 = __this->get_U24disposing_2();
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0051:
	{
		goto IL_006a;
	}

IL_0056:
	{
		LaserController_t2836074815 * L_6 = __this->get_U24this_0();
		LaserController_HideLaser_m4253751403(L_6, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0068:
	{
		return (bool)0;
	}

IL_006a:
	{
		return (bool)1;
	}
}
// System.Object LaserController/<ShowLaser>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowLaserU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2907307706 (U3CShowLaserU3Ec__Iterator0_t1127661705 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object LaserController/<ShowLaser>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowLaserU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m180913378 (U3CShowLaserU3Ec__Iterator0_t1127661705 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void LaserController/<ShowLaser>c__Iterator0::Dispose()
extern "C"  void U3CShowLaserU3Ec__Iterator0_Dispose_m3957859483 (U3CShowLaserU3Ec__Iterator0_t1127661705 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void LaserController/<ShowLaser>c__Iterator0::Reset()
extern "C"  void U3CShowLaserU3Ec__Iterator0_Reset_m2880137833 (U3CShowLaserU3Ec__Iterator0_t1127661705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowLaserU3Ec__Iterator0_Reset_m2880137833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void LevelGenerator::.ctor()
extern "C"  void LevelGenerator__ctor_m4211300706 (LevelGenerator_t1121608959 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LevelGenerator::Start()
extern "C"  void LevelGenerator_Start_m4240987518 (LevelGenerator_t1121608959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelGenerator_Start_m4240987518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3811852957(&L_0, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_obstacleColor_12(L_0);
		Color_t2020392075  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m3811852957(&L_1, (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_powerupColor_13(L_1);
		Color_t2020392075  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Color__ctor_m3811852957(&L_2, (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_collectableColor_14(L_2);
		Color_t2020392075  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m3811852957(&L_3, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_finishColor_15(L_3);
		Color_t2020392075  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m3811852957(&L_4, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_capeColor_17(L_4);
		Color_t2020392075  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m3811852957(&L_5, (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_eyeColor_16(L_5);
		int32_t L_6 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral3196376945, 1, /*hidden argument*/NULL);
		((LevelGenerator_t1121608959_StaticFields*)LevelGenerator_t1121608959_il2cpp_TypeInfo_var->static_fields)->set_level_18(L_6);
		int32_t L_7 = ((LevelGenerator_t1121608959_StaticFields*)LevelGenerator_t1121608959_il2cpp_TypeInfo_var->static_fields)->get_level_18();
		V_0 = ((int32_t)((int32_t)L_7-(int32_t)1));
		int32_t L_8 = V_0;
		LevelGenerator_GenerateLevel_m1877206344(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		if (L_9)
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_10 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral3015339463, 0, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_10) == ((int32_t)1))? 1 : 0);
		bool L_11 = V_1;
		if (!L_11)
		{
			goto IL_00e6;
		}
	}
	{
		InstructionsController_t1323375089 * L_12 = __this->get_instructions_11();
		InstructionsController_ShowWallInstructions_m683673258(L_12, /*hidden argument*/NULL);
		goto IL_00f1;
	}

IL_00e6:
	{
		InstructionsController_t1323375089 * L_13 = __this->get_instructions_11();
		InstructionsController_ShowJumpInstructions_m34564830(L_13, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		GameObject_t1756533147 * L_14 = __this->get_eye_2();
		GameObject_SetActive_m2887581199(L_14, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = __this->get_cape_3();
		GameObject_SetActive_m2887581199(L_15, (bool)0, /*hidden argument*/NULL);
	}

IL_0109:
	{
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral3015339463, 0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2002042674, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LevelGenerator::GenerateLevel(System.Int32)
extern "C"  void LevelGenerator_GenerateLevel_m1877206344 (LevelGenerator_t1121608959 * __this, int32_t ___level0, const MethodInfo* method)
{
	Texture2D_t3542995729 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Color_t2020392075  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Texture2DU5BU5D_t2724090252* L_0 = __this->get_maps_4();
		int32_t L_1 = ___level0;
		int32_t L_2 = L_1;
		Texture2D_t3542995729 * L_3 = (L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = L_3;
		V_1 = 0;
		goto IL_0053;
	}

IL_0010:
	{
		V_2 = 0;
		goto IL_0043;
	}

IL_0017:
	{
		Texture2D_t3542995729 * L_4 = V_0;
		int32_t L_5 = V_1;
		int32_t L_6 = V_2;
		Color_t2020392075  L_7 = Texture2D_GetPixel_m1570987051(L_4, L_5, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		float L_8 = (&V_3)->get_a_3();
		if ((!(((float)L_8) == ((float)(0.0f)))))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_003f;
	}

IL_0036:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = V_2;
		Color_t2020392075  L_11 = V_3;
		LevelGenerator_GenerateTile_m2057031207(__this, L_9, L_10, L_11, /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_13 = V_2;
		Texture2D_t3542995729 * L_14 = V_0;
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_14);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0053:
	{
		int32_t L_17 = V_1;
		Texture2D_t3542995729 * L_18 = V_0;
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_18);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// System.Void LevelGenerator::GenerateTile(System.Int32,System.Int32,UnityEngine.Color)
extern "C"  void LevelGenerator_GenerateTile_m2057031207 (LevelGenerator_t1121608959 * __this, int32_t ___x0, int32_t ___y1, Color_t2020392075  ___pixelColor2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelGenerator_GenerateTile_m2057031207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = ___y1;
		V_0 = ((float)((float)(((float)((float)L_0)))-(float)(3.5f)));
		int32_t L_1 = ___x0;
		V_1 = (((float)((float)((int32_t)((int32_t)2*(int32_t)L_1)))));
		float L_2 = V_1;
		float L_3 = V_0;
		Vector2__ctor_m3067419446((&V_2), L_2, L_3, /*hidden argument*/NULL);
		Color_t2020392075  L_4 = __this->get_obstacleColor_12();
		Color_t2020392075  L_5 = L_4;
		Il2CppObject * L_6 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_5);
		bool L_7 = Color_Equals_m661618137((&___pixelColor2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		GameObject_t1756533147 * L_8 = __this->get_obstaclePrefab_5();
		Vector2_t2243707579  L_9 = V_2;
		Vector3_t2243707580  L_10 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_11 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m351711267(NULL /*static, unused*/, L_8, L_10, L_11, L_12, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m351711267_MethodInfo_var);
	}

IL_0051:
	{
		Color_t2020392075  L_13 = __this->get_powerupColor_13();
		Color_t2020392075  L_14 = L_13;
		Il2CppObject * L_15 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Color_Equals_m661618137((&___pixelColor2), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0096;
		}
	}
	{
		GameObject_t1756533147 * L_17 = __this->get_powerupPrefab_6();
		Vector2_t2243707579  L_18 = V_2;
		Vector3_t2243707580  L_19 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = __this->get_powerupPrefab_6();
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_22 = Transform_get_rotation_m1033555130(L_21, /*hidden argument*/NULL);
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m351711267(NULL /*static, unused*/, L_17, L_19, L_22, L_23, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m351711267_MethodInfo_var);
	}

IL_0096:
	{
		Color_t2020392075  L_24 = __this->get_collectableColor_14();
		Color_t2020392075  L_25 = L_24;
		Il2CppObject * L_26 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_25);
		bool L_27 = Color_Equals_m661618137((&___pixelColor2), L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00b3;
		}
	}

IL_00b3:
	{
		Color_t2020392075  L_28 = __this->get_finishColor_15();
		Color_t2020392075  L_29 = L_28;
		Il2CppObject * L_30 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_29);
		bool L_31 = Color_Equals_m661618137((&___pixelColor2), L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0104;
		}
	}
	{
		(&V_2)->set_y_1((-4.0f));
		GameObject_t1756533147 * L_32 = __this->get_finishPrefab_8();
		Vector2_t2243707579  L_33 = V_2;
		Vector3_t2243707580  L_34 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = __this->get_finishPrefab_8();
		Transform_t3275118058 * L_36 = GameObject_get_transform_m909382139(L_35, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_37 = Transform_get_rotation_m1033555130(L_36, /*hidden argument*/NULL);
		Transform_t3275118058 * L_38 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m351711267(NULL /*static, unused*/, L_32, L_34, L_37, L_38, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m351711267_MethodInfo_var);
	}

IL_0104:
	{
		Color_t2020392075  L_39 = __this->get_eyeColor_16();
		Color_t2020392075  L_40 = L_39;
		Il2CppObject * L_41 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_40);
		bool L_42 = Color_Equals_m661618137((&___pixelColor2), L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0149;
		}
	}
	{
		GameObject_t1756533147 * L_43 = __this->get_eyePrefab_10();
		Vector2_t2243707579  L_44 = V_2;
		Vector3_t2243707580  L_45 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_46 = __this->get_eyePrefab_10();
		Transform_t3275118058 * L_47 = GameObject_get_transform_m909382139(L_46, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_48 = Transform_get_rotation_m1033555130(L_47, /*hidden argument*/NULL);
		Transform_t3275118058 * L_49 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m351711267(NULL /*static, unused*/, L_43, L_45, L_48, L_49, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m351711267_MethodInfo_var);
	}

IL_0149:
	{
		Color_t2020392075  L_50 = __this->get_capeColor_17();
		Color_t2020392075  L_51 = L_50;
		Il2CppObject * L_52 = Box(Color_t2020392075_il2cpp_TypeInfo_var, &L_51);
		bool L_53 = Color_Equals_m661618137((&___pixelColor2), L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_018e;
		}
	}
	{
		GameObject_t1756533147 * L_54 = __this->get_capePrefab_9();
		Vector2_t2243707579  L_55 = V_2;
		Vector3_t2243707580  L_56 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_57 = __this->get_capePrefab_9();
		Transform_t3275118058 * L_58 = GameObject_get_transform_m909382139(L_57, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_59 = Transform_get_rotation_m1033555130(L_58, /*hidden argument*/NULL);
		Transform_t3275118058 * L_60 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m351711267(NULL /*static, unused*/, L_54, L_56, L_59, L_60, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m351711267_MethodInfo_var);
	}

IL_018e:
	{
		return;
	}
}
// System.Void MenuController::.ctor()
extern "C"  void MenuController__ctor_m1077658770 (MenuController_t848154101 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuController::Start()
extern "C"  void MenuController_Start_m3141389506 (MenuController_t848154101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuController_Start_m3141389506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Image_t2042527209 * G_B10_0 = NULL;
	Image_t2042527209 * G_B9_0 = NULL;
	Color_t2020392075  G_B11_0;
	memset(&G_B11_0, 0, sizeof(G_B11_0));
	Image_t2042527209 * G_B11_1 = NULL;
	{
		Color_t2020392075 * L_0 = __this->get_address_of_completeColor_7();
		ColorUtility_TryParseHtmlString_m3515386476(NULL /*static, unused*/, _stringLiteral3912186174, L_0, /*hidden argument*/NULL);
		Color_t2020392075 * L_1 = __this->get_address_of_nextColor_8();
		ColorUtility_TryParseHtmlString_m3515386476(NULL /*static, unused*/, _stringLiteral3335872661, L_1, /*hidden argument*/NULL);
		Color_t2020392075 * L_2 = __this->get_address_of_futureColor_9();
		ColorUtility_TryParseHtmlString_m3515386476(NULL /*static, unused*/, _stringLiteral549601424, L_2, /*hidden argument*/NULL);
		int32_t L_3 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral120639914, 0, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0063;
		}
	}
	{
		GameObject_t1756533147 * L_5 = __this->get_btn1_2();
		Image_t2042527209 * L_6 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_5, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		Color_t2020392075  L_7 = __this->get_nextColor_8();
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_6, L_7);
		goto IL_0149;
	}

IL_0063:
	{
		GameObject_t1756533147 * L_8 = __this->get_btn1_2();
		Image_t2042527209 * L_9 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_8, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		Color_t2020392075  L_10 = __this->get_completeColor_7();
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_10);
		int32_t L_11 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)2))))
		{
			goto IL_009b;
		}
	}
	{
		GameObject_t1756533147 * L_12 = __this->get_btn2_3();
		Image_t2042527209 * L_13 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		Color_t2020392075  L_14 = __this->get_nextColor_8();
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_14);
		goto IL_0149;
	}

IL_009b:
	{
		GameObject_t1756533147 * L_15 = __this->get_btn2_3();
		Image_t2042527209 * L_16 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_15, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		Color_t2020392075  L_17 = __this->get_completeColor_7();
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_16, L_17);
		int32_t L_18 = V_0;
		if ((!(((uint32_t)L_18) == ((uint32_t)3))))
		{
			goto IL_00d3;
		}
	}
	{
		GameObject_t1756533147 * L_19 = __this->get_btn3_4();
		Image_t2042527209 * L_20 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_19, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		Color_t2020392075  L_21 = __this->get_nextColor_8();
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_20, L_21);
		goto IL_0149;
	}

IL_00d3:
	{
		GameObject_t1756533147 * L_22 = __this->get_btn3_4();
		Image_t2042527209 * L_23 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_22, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		Color_t2020392075  L_24 = __this->get_completeColor_7();
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_23, L_24);
		int32_t L_25 = V_0;
		if ((!(((uint32_t)L_25) == ((uint32_t)4))))
		{
			goto IL_010b;
		}
	}
	{
		GameObject_t1756533147 * L_26 = __this->get_btn4_5();
		Image_t2042527209 * L_27 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_26, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		Color_t2020392075  L_28 = __this->get_nextColor_8();
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_27, L_28);
		goto IL_0149;
	}

IL_010b:
	{
		GameObject_t1756533147 * L_29 = __this->get_btn4_5();
		Image_t2042527209 * L_30 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_29, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		Color_t2020392075  L_31 = __this->get_completeColor_7();
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_30, L_31);
		GameObject_t1756533147 * L_32 = __this->get_btn5_6();
		Image_t2042527209 * L_33 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_32, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		int32_t L_34 = V_0;
		G_B9_0 = L_33;
		if ((!(((uint32_t)L_34) == ((uint32_t)5))))
		{
			G_B10_0 = L_33;
			goto IL_013e;
		}
	}
	{
		Color_t2020392075  L_35 = __this->get_nextColor_8();
		G_B11_0 = L_35;
		G_B11_1 = G_B9_0;
		goto IL_0144;
	}

IL_013e:
	{
		Color_t2020392075  L_36 = __this->get_completeColor_7();
		G_B11_0 = L_36;
		G_B11_1 = G_B10_0;
	}

IL_0144:
	{
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, G_B11_1, G_B11_0);
	}

IL_0149:
	{
		return;
	}
}
// System.Void MenuController::StartLevel(System.Int32)
extern "C"  void MenuController_StartLevel_m1694854699 (MenuController_t848154101 * __this, int32_t ___level0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuController_StartLevel_m1694854699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___level0;
		int32_t L_1 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral120639914, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)((int32_t)L_1+(int32_t)1)))))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_2 = ___level0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral24946573, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_002d:
	{
		int32_t L_6 = ___level0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral3196376945, L_6, /*hidden argument*/NULL);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2328219732, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void Mover::.ctor()
extern "C"  void Mover__ctor_m2062620528 (Mover_t873752897 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (6.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_movement_2(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mover::Update()
extern "C"  void Mover_Update_m423995237 (Mover_t873752897 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_1 = __this->get_movement_2();
		Vector2_t2243707579  L_2 = Vector2_op_Multiply_m3393065202(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Transform_get_localPosition_m2533925116(L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = V_0;
		Vector2_t2243707579  L_8 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Transform_set_localPosition_m1026930133(L_3, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObstacleController::.ctor()
extern "C"  void ObstacleController__ctor_m4054730656 (ObstacleController_t2319665031 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObstacleController::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void ObstacleController_OnCollisionEnter2D_m1129342162 (ObstacleController_t2319665031 * __this, Collision2D_t1539500754 * ___collision0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObstacleController_OnCollisionEnter2D_m1129342162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Collision2D_t1539500754 * L_0 = ___collision0;
		GameObject_t1756533147 * L_1 = Collision2D_get_gameObject_m4234358314(L_0, /*hidden argument*/NULL);
		bool L_2 = GameObject_CompareTag_m2797152613(L_1, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		Scene_t1684909666  L_3 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = Scene_get_name_m745914591((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral3015339463, 1, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void PowerupController::.ctor()
extern "C"  void PowerupController__ctor_m3285707397 (PowerupController_t1946057392 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PowerupController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void PowerupController_OnTriggerEnter2D_m1219282109 (PowerupController_t1946057392 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PowerupController_OnTriggerEnter2D_m1219282109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	HeroController_t4041626604 * V_1 = NULL;
	{
		Collider2D_t646061738 * L_0 = ___other0;
		bool L_1 = Component_CompareTag_m3443292365(L_0, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_2 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral2002042674, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2002042674, ((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/NULL);
		Collider2D_t646061738 * L_4 = ___other0;
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Transform_get_parent_m147407266(L_5, /*hidden argument*/NULL);
		HeroController_t4041626604 * L_7 = Component_GetComponent_TisHeroController_t4041626604_m2559195979(L_6, /*hidden argument*/Component_GetComponent_TisHeroController_t4041626604_m2559195979_MethodInfo_var);
		V_1 = L_7;
		HeroController_t4041626604 * L_8 = V_1;
		HeroController_ReplenishPower_m2136775080(L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
