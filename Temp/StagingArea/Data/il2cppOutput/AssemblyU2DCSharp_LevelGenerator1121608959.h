﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2724090252;
// InstructionsController
struct InstructionsController_t1323375089;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelGenerator
struct  LevelGenerator_t1121608959  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject LevelGenerator::eye
	GameObject_t1756533147 * ___eye_2;
	// UnityEngine.GameObject LevelGenerator::cape
	GameObject_t1756533147 * ___cape_3;
	// UnityEngine.Texture2D[] LevelGenerator::maps
	Texture2DU5BU5D_t2724090252* ___maps_4;
	// UnityEngine.GameObject LevelGenerator::obstaclePrefab
	GameObject_t1756533147 * ___obstaclePrefab_5;
	// UnityEngine.GameObject LevelGenerator::powerupPrefab
	GameObject_t1756533147 * ___powerupPrefab_6;
	// UnityEngine.GameObject LevelGenerator::collectablePrefab
	GameObject_t1756533147 * ___collectablePrefab_7;
	// UnityEngine.GameObject LevelGenerator::finishPrefab
	GameObject_t1756533147 * ___finishPrefab_8;
	// UnityEngine.GameObject LevelGenerator::capePrefab
	GameObject_t1756533147 * ___capePrefab_9;
	// UnityEngine.GameObject LevelGenerator::eyePrefab
	GameObject_t1756533147 * ___eyePrefab_10;
	// InstructionsController LevelGenerator::instructions
	InstructionsController_t1323375089 * ___instructions_11;
	// UnityEngine.Color LevelGenerator::obstacleColor
	Color_t2020392075  ___obstacleColor_12;
	// UnityEngine.Color LevelGenerator::powerupColor
	Color_t2020392075  ___powerupColor_13;
	// UnityEngine.Color LevelGenerator::collectableColor
	Color_t2020392075  ___collectableColor_14;
	// UnityEngine.Color LevelGenerator::finishColor
	Color_t2020392075  ___finishColor_15;
	// UnityEngine.Color LevelGenerator::eyeColor
	Color_t2020392075  ___eyeColor_16;
	// UnityEngine.Color LevelGenerator::capeColor
	Color_t2020392075  ___capeColor_17;

public:
	inline static int32_t get_offset_of_eye_2() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___eye_2)); }
	inline GameObject_t1756533147 * get_eye_2() const { return ___eye_2; }
	inline GameObject_t1756533147 ** get_address_of_eye_2() { return &___eye_2; }
	inline void set_eye_2(GameObject_t1756533147 * value)
	{
		___eye_2 = value;
		Il2CppCodeGenWriteBarrier(&___eye_2, value);
	}

	inline static int32_t get_offset_of_cape_3() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___cape_3)); }
	inline GameObject_t1756533147 * get_cape_3() const { return ___cape_3; }
	inline GameObject_t1756533147 ** get_address_of_cape_3() { return &___cape_3; }
	inline void set_cape_3(GameObject_t1756533147 * value)
	{
		___cape_3 = value;
		Il2CppCodeGenWriteBarrier(&___cape_3, value);
	}

	inline static int32_t get_offset_of_maps_4() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___maps_4)); }
	inline Texture2DU5BU5D_t2724090252* get_maps_4() const { return ___maps_4; }
	inline Texture2DU5BU5D_t2724090252** get_address_of_maps_4() { return &___maps_4; }
	inline void set_maps_4(Texture2DU5BU5D_t2724090252* value)
	{
		___maps_4 = value;
		Il2CppCodeGenWriteBarrier(&___maps_4, value);
	}

	inline static int32_t get_offset_of_obstaclePrefab_5() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___obstaclePrefab_5)); }
	inline GameObject_t1756533147 * get_obstaclePrefab_5() const { return ___obstaclePrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_obstaclePrefab_5() { return &___obstaclePrefab_5; }
	inline void set_obstaclePrefab_5(GameObject_t1756533147 * value)
	{
		___obstaclePrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___obstaclePrefab_5, value);
	}

	inline static int32_t get_offset_of_powerupPrefab_6() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___powerupPrefab_6)); }
	inline GameObject_t1756533147 * get_powerupPrefab_6() const { return ___powerupPrefab_6; }
	inline GameObject_t1756533147 ** get_address_of_powerupPrefab_6() { return &___powerupPrefab_6; }
	inline void set_powerupPrefab_6(GameObject_t1756533147 * value)
	{
		___powerupPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___powerupPrefab_6, value);
	}

	inline static int32_t get_offset_of_collectablePrefab_7() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___collectablePrefab_7)); }
	inline GameObject_t1756533147 * get_collectablePrefab_7() const { return ___collectablePrefab_7; }
	inline GameObject_t1756533147 ** get_address_of_collectablePrefab_7() { return &___collectablePrefab_7; }
	inline void set_collectablePrefab_7(GameObject_t1756533147 * value)
	{
		___collectablePrefab_7 = value;
		Il2CppCodeGenWriteBarrier(&___collectablePrefab_7, value);
	}

	inline static int32_t get_offset_of_finishPrefab_8() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___finishPrefab_8)); }
	inline GameObject_t1756533147 * get_finishPrefab_8() const { return ___finishPrefab_8; }
	inline GameObject_t1756533147 ** get_address_of_finishPrefab_8() { return &___finishPrefab_8; }
	inline void set_finishPrefab_8(GameObject_t1756533147 * value)
	{
		___finishPrefab_8 = value;
		Il2CppCodeGenWriteBarrier(&___finishPrefab_8, value);
	}

	inline static int32_t get_offset_of_capePrefab_9() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___capePrefab_9)); }
	inline GameObject_t1756533147 * get_capePrefab_9() const { return ___capePrefab_9; }
	inline GameObject_t1756533147 ** get_address_of_capePrefab_9() { return &___capePrefab_9; }
	inline void set_capePrefab_9(GameObject_t1756533147 * value)
	{
		___capePrefab_9 = value;
		Il2CppCodeGenWriteBarrier(&___capePrefab_9, value);
	}

	inline static int32_t get_offset_of_eyePrefab_10() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___eyePrefab_10)); }
	inline GameObject_t1756533147 * get_eyePrefab_10() const { return ___eyePrefab_10; }
	inline GameObject_t1756533147 ** get_address_of_eyePrefab_10() { return &___eyePrefab_10; }
	inline void set_eyePrefab_10(GameObject_t1756533147 * value)
	{
		___eyePrefab_10 = value;
		Il2CppCodeGenWriteBarrier(&___eyePrefab_10, value);
	}

	inline static int32_t get_offset_of_instructions_11() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___instructions_11)); }
	inline InstructionsController_t1323375089 * get_instructions_11() const { return ___instructions_11; }
	inline InstructionsController_t1323375089 ** get_address_of_instructions_11() { return &___instructions_11; }
	inline void set_instructions_11(InstructionsController_t1323375089 * value)
	{
		___instructions_11 = value;
		Il2CppCodeGenWriteBarrier(&___instructions_11, value);
	}

	inline static int32_t get_offset_of_obstacleColor_12() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___obstacleColor_12)); }
	inline Color_t2020392075  get_obstacleColor_12() const { return ___obstacleColor_12; }
	inline Color_t2020392075 * get_address_of_obstacleColor_12() { return &___obstacleColor_12; }
	inline void set_obstacleColor_12(Color_t2020392075  value)
	{
		___obstacleColor_12 = value;
	}

	inline static int32_t get_offset_of_powerupColor_13() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___powerupColor_13)); }
	inline Color_t2020392075  get_powerupColor_13() const { return ___powerupColor_13; }
	inline Color_t2020392075 * get_address_of_powerupColor_13() { return &___powerupColor_13; }
	inline void set_powerupColor_13(Color_t2020392075  value)
	{
		___powerupColor_13 = value;
	}

	inline static int32_t get_offset_of_collectableColor_14() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___collectableColor_14)); }
	inline Color_t2020392075  get_collectableColor_14() const { return ___collectableColor_14; }
	inline Color_t2020392075 * get_address_of_collectableColor_14() { return &___collectableColor_14; }
	inline void set_collectableColor_14(Color_t2020392075  value)
	{
		___collectableColor_14 = value;
	}

	inline static int32_t get_offset_of_finishColor_15() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___finishColor_15)); }
	inline Color_t2020392075  get_finishColor_15() const { return ___finishColor_15; }
	inline Color_t2020392075 * get_address_of_finishColor_15() { return &___finishColor_15; }
	inline void set_finishColor_15(Color_t2020392075  value)
	{
		___finishColor_15 = value;
	}

	inline static int32_t get_offset_of_eyeColor_16() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___eyeColor_16)); }
	inline Color_t2020392075  get_eyeColor_16() const { return ___eyeColor_16; }
	inline Color_t2020392075 * get_address_of_eyeColor_16() { return &___eyeColor_16; }
	inline void set_eyeColor_16(Color_t2020392075  value)
	{
		___eyeColor_16 = value;
	}

	inline static int32_t get_offset_of_capeColor_17() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959, ___capeColor_17)); }
	inline Color_t2020392075  get_capeColor_17() const { return ___capeColor_17; }
	inline Color_t2020392075 * get_address_of_capeColor_17() { return &___capeColor_17; }
	inline void set_capeColor_17(Color_t2020392075  value)
	{
		___capeColor_17 = value;
	}
};

struct LevelGenerator_t1121608959_StaticFields
{
public:
	// System.Int32 LevelGenerator::level
	int32_t ___level_18;

public:
	inline static int32_t get_offset_of_level_18() { return static_cast<int32_t>(offsetof(LevelGenerator_t1121608959_StaticFields, ___level_18)); }
	inline int32_t get_level_18() const { return ___level_18; }
	inline int32_t* get_address_of_level_18() { return &___level_18; }
	inline void set_level_18(int32_t value)
	{
		___level_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
