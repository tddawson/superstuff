﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// HeroController
struct HeroController_t4041626604;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t3607102586  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GameController::isMobile
	bool ___isMobile_2;
	// UnityEngine.GameObject GameController::heroContainer
	GameObject_t1756533147 * ___heroContainer_3;
	// HeroController GameController::heroController
	HeroController_t4041626604 * ___heroController_4;

public:
	inline static int32_t get_offset_of_isMobile_2() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___isMobile_2)); }
	inline bool get_isMobile_2() const { return ___isMobile_2; }
	inline bool* get_address_of_isMobile_2() { return &___isMobile_2; }
	inline void set_isMobile_2(bool value)
	{
		___isMobile_2 = value;
	}

	inline static int32_t get_offset_of_heroContainer_3() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___heroContainer_3)); }
	inline GameObject_t1756533147 * get_heroContainer_3() const { return ___heroContainer_3; }
	inline GameObject_t1756533147 ** get_address_of_heroContainer_3() { return &___heroContainer_3; }
	inline void set_heroContainer_3(GameObject_t1756533147 * value)
	{
		___heroContainer_3 = value;
		Il2CppCodeGenWriteBarrier(&___heroContainer_3, value);
	}

	inline static int32_t get_offset_of_heroController_4() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___heroController_4)); }
	inline HeroController_t4041626604 * get_heroController_4() const { return ___heroController_4; }
	inline HeroController_t4041626604 ** get_address_of_heroController_4() { return &___heroController_4; }
	inline void set_heroController_4(HeroController_t4041626604 * value)
	{
		___heroController_4 = value;
		Il2CppCodeGenWriteBarrier(&___heroController_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
