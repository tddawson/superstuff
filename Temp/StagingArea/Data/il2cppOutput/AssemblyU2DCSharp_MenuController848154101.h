﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuController
struct  MenuController_t848154101  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MenuController::btn1
	GameObject_t1756533147 * ___btn1_2;
	// UnityEngine.GameObject MenuController::btn2
	GameObject_t1756533147 * ___btn2_3;
	// UnityEngine.GameObject MenuController::btn3
	GameObject_t1756533147 * ___btn3_4;
	// UnityEngine.GameObject MenuController::btn4
	GameObject_t1756533147 * ___btn4_5;
	// UnityEngine.GameObject MenuController::btn5
	GameObject_t1756533147 * ___btn5_6;
	// UnityEngine.Color MenuController::completeColor
	Color_t2020392075  ___completeColor_7;
	// UnityEngine.Color MenuController::nextColor
	Color_t2020392075  ___nextColor_8;
	// UnityEngine.Color MenuController::futureColor
	Color_t2020392075  ___futureColor_9;

public:
	inline static int32_t get_offset_of_btn1_2() { return static_cast<int32_t>(offsetof(MenuController_t848154101, ___btn1_2)); }
	inline GameObject_t1756533147 * get_btn1_2() const { return ___btn1_2; }
	inline GameObject_t1756533147 ** get_address_of_btn1_2() { return &___btn1_2; }
	inline void set_btn1_2(GameObject_t1756533147 * value)
	{
		___btn1_2 = value;
		Il2CppCodeGenWriteBarrier(&___btn1_2, value);
	}

	inline static int32_t get_offset_of_btn2_3() { return static_cast<int32_t>(offsetof(MenuController_t848154101, ___btn2_3)); }
	inline GameObject_t1756533147 * get_btn2_3() const { return ___btn2_3; }
	inline GameObject_t1756533147 ** get_address_of_btn2_3() { return &___btn2_3; }
	inline void set_btn2_3(GameObject_t1756533147 * value)
	{
		___btn2_3 = value;
		Il2CppCodeGenWriteBarrier(&___btn2_3, value);
	}

	inline static int32_t get_offset_of_btn3_4() { return static_cast<int32_t>(offsetof(MenuController_t848154101, ___btn3_4)); }
	inline GameObject_t1756533147 * get_btn3_4() const { return ___btn3_4; }
	inline GameObject_t1756533147 ** get_address_of_btn3_4() { return &___btn3_4; }
	inline void set_btn3_4(GameObject_t1756533147 * value)
	{
		___btn3_4 = value;
		Il2CppCodeGenWriteBarrier(&___btn3_4, value);
	}

	inline static int32_t get_offset_of_btn4_5() { return static_cast<int32_t>(offsetof(MenuController_t848154101, ___btn4_5)); }
	inline GameObject_t1756533147 * get_btn4_5() const { return ___btn4_5; }
	inline GameObject_t1756533147 ** get_address_of_btn4_5() { return &___btn4_5; }
	inline void set_btn4_5(GameObject_t1756533147 * value)
	{
		___btn4_5 = value;
		Il2CppCodeGenWriteBarrier(&___btn4_5, value);
	}

	inline static int32_t get_offset_of_btn5_6() { return static_cast<int32_t>(offsetof(MenuController_t848154101, ___btn5_6)); }
	inline GameObject_t1756533147 * get_btn5_6() const { return ___btn5_6; }
	inline GameObject_t1756533147 ** get_address_of_btn5_6() { return &___btn5_6; }
	inline void set_btn5_6(GameObject_t1756533147 * value)
	{
		___btn5_6 = value;
		Il2CppCodeGenWriteBarrier(&___btn5_6, value);
	}

	inline static int32_t get_offset_of_completeColor_7() { return static_cast<int32_t>(offsetof(MenuController_t848154101, ___completeColor_7)); }
	inline Color_t2020392075  get_completeColor_7() const { return ___completeColor_7; }
	inline Color_t2020392075 * get_address_of_completeColor_7() { return &___completeColor_7; }
	inline void set_completeColor_7(Color_t2020392075  value)
	{
		___completeColor_7 = value;
	}

	inline static int32_t get_offset_of_nextColor_8() { return static_cast<int32_t>(offsetof(MenuController_t848154101, ___nextColor_8)); }
	inline Color_t2020392075  get_nextColor_8() const { return ___nextColor_8; }
	inline Color_t2020392075 * get_address_of_nextColor_8() { return &___nextColor_8; }
	inline void set_nextColor_8(Color_t2020392075  value)
	{
		___nextColor_8 = value;
	}

	inline static int32_t get_offset_of_futureColor_9() { return static_cast<int32_t>(offsetof(MenuController_t848154101, ___futureColor_9)); }
	inline Color_t2020392075  get_futureColor_9() const { return ___futureColor_9; }
	inline Color_t2020392075 * get_address_of_futureColor_9() { return &___futureColor_9; }
	inline void set_futureColor_9(Color_t2020392075  value)
	{
		___futureColor_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
