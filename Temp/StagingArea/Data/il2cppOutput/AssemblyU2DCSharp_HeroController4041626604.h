﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// LaserController
struct LaserController_t2836074815;
// InstructionsController
struct InstructionsController_t1323375089;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroController
struct  HeroController_t4041626604  : public MonoBehaviour_t1158329972
{
public:
	// System.Single HeroController::jumpPower
	float ___jumpPower_2;
	// UnityEngine.GameObject HeroController::body
	GameObject_t1756533147 * ___body_3;
	// UnityEngine.GameObject HeroController::eye
	GameObject_t1756533147 * ___eye_4;
	// UnityEngine.GameObject HeroController::cape
	GameObject_t1756533147 * ___cape_5;
	// System.Single HeroController::maxPower
	float ___maxPower_6;
	// System.Single HeroController::powerToJump
	float ___powerToJump_7;
	// System.Single HeroController::powerToShoot
	float ___powerToShoot_8;
	// System.Single HeroController::powerRemaining
	float ___powerRemaining_9;
	// System.Boolean HeroController::hasFlight
	bool ___hasFlight_10;
	// System.Boolean HeroController::hasLaserVision
	bool ___hasLaserVision_11;
	// UnityEngine.RectTransform HeroController::powerMeter
	RectTransform_t3349966182 * ___powerMeter_12;
	// System.Boolean HeroController::isOnFloor
	bool ___isOnFloor_13;
	// UnityEngine.GameObject HeroController::laser
	GameObject_t1756533147 * ___laser_14;
	// UnityEngine.LineRenderer HeroController::laserRenderer
	LineRenderer_t849157671 * ___laserRenderer_15;
	// LaserController HeroController::laserController
	LaserController_t2836074815 * ___laserController_16;
	// InstructionsController HeroController::instructions
	InstructionsController_t1323375089 * ___instructions_17;

public:
	inline static int32_t get_offset_of_jumpPower_2() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___jumpPower_2)); }
	inline float get_jumpPower_2() const { return ___jumpPower_2; }
	inline float* get_address_of_jumpPower_2() { return &___jumpPower_2; }
	inline void set_jumpPower_2(float value)
	{
		___jumpPower_2 = value;
	}

	inline static int32_t get_offset_of_body_3() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___body_3)); }
	inline GameObject_t1756533147 * get_body_3() const { return ___body_3; }
	inline GameObject_t1756533147 ** get_address_of_body_3() { return &___body_3; }
	inline void set_body_3(GameObject_t1756533147 * value)
	{
		___body_3 = value;
		Il2CppCodeGenWriteBarrier(&___body_3, value);
	}

	inline static int32_t get_offset_of_eye_4() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___eye_4)); }
	inline GameObject_t1756533147 * get_eye_4() const { return ___eye_4; }
	inline GameObject_t1756533147 ** get_address_of_eye_4() { return &___eye_4; }
	inline void set_eye_4(GameObject_t1756533147 * value)
	{
		___eye_4 = value;
		Il2CppCodeGenWriteBarrier(&___eye_4, value);
	}

	inline static int32_t get_offset_of_cape_5() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___cape_5)); }
	inline GameObject_t1756533147 * get_cape_5() const { return ___cape_5; }
	inline GameObject_t1756533147 ** get_address_of_cape_5() { return &___cape_5; }
	inline void set_cape_5(GameObject_t1756533147 * value)
	{
		___cape_5 = value;
		Il2CppCodeGenWriteBarrier(&___cape_5, value);
	}

	inline static int32_t get_offset_of_maxPower_6() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___maxPower_6)); }
	inline float get_maxPower_6() const { return ___maxPower_6; }
	inline float* get_address_of_maxPower_6() { return &___maxPower_6; }
	inline void set_maxPower_6(float value)
	{
		___maxPower_6 = value;
	}

	inline static int32_t get_offset_of_powerToJump_7() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___powerToJump_7)); }
	inline float get_powerToJump_7() const { return ___powerToJump_7; }
	inline float* get_address_of_powerToJump_7() { return &___powerToJump_7; }
	inline void set_powerToJump_7(float value)
	{
		___powerToJump_7 = value;
	}

	inline static int32_t get_offset_of_powerToShoot_8() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___powerToShoot_8)); }
	inline float get_powerToShoot_8() const { return ___powerToShoot_8; }
	inline float* get_address_of_powerToShoot_8() { return &___powerToShoot_8; }
	inline void set_powerToShoot_8(float value)
	{
		___powerToShoot_8 = value;
	}

	inline static int32_t get_offset_of_powerRemaining_9() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___powerRemaining_9)); }
	inline float get_powerRemaining_9() const { return ___powerRemaining_9; }
	inline float* get_address_of_powerRemaining_9() { return &___powerRemaining_9; }
	inline void set_powerRemaining_9(float value)
	{
		___powerRemaining_9 = value;
	}

	inline static int32_t get_offset_of_hasFlight_10() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___hasFlight_10)); }
	inline bool get_hasFlight_10() const { return ___hasFlight_10; }
	inline bool* get_address_of_hasFlight_10() { return &___hasFlight_10; }
	inline void set_hasFlight_10(bool value)
	{
		___hasFlight_10 = value;
	}

	inline static int32_t get_offset_of_hasLaserVision_11() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___hasLaserVision_11)); }
	inline bool get_hasLaserVision_11() const { return ___hasLaserVision_11; }
	inline bool* get_address_of_hasLaserVision_11() { return &___hasLaserVision_11; }
	inline void set_hasLaserVision_11(bool value)
	{
		___hasLaserVision_11 = value;
	}

	inline static int32_t get_offset_of_powerMeter_12() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___powerMeter_12)); }
	inline RectTransform_t3349966182 * get_powerMeter_12() const { return ___powerMeter_12; }
	inline RectTransform_t3349966182 ** get_address_of_powerMeter_12() { return &___powerMeter_12; }
	inline void set_powerMeter_12(RectTransform_t3349966182 * value)
	{
		___powerMeter_12 = value;
		Il2CppCodeGenWriteBarrier(&___powerMeter_12, value);
	}

	inline static int32_t get_offset_of_isOnFloor_13() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___isOnFloor_13)); }
	inline bool get_isOnFloor_13() const { return ___isOnFloor_13; }
	inline bool* get_address_of_isOnFloor_13() { return &___isOnFloor_13; }
	inline void set_isOnFloor_13(bool value)
	{
		___isOnFloor_13 = value;
	}

	inline static int32_t get_offset_of_laser_14() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___laser_14)); }
	inline GameObject_t1756533147 * get_laser_14() const { return ___laser_14; }
	inline GameObject_t1756533147 ** get_address_of_laser_14() { return &___laser_14; }
	inline void set_laser_14(GameObject_t1756533147 * value)
	{
		___laser_14 = value;
		Il2CppCodeGenWriteBarrier(&___laser_14, value);
	}

	inline static int32_t get_offset_of_laserRenderer_15() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___laserRenderer_15)); }
	inline LineRenderer_t849157671 * get_laserRenderer_15() const { return ___laserRenderer_15; }
	inline LineRenderer_t849157671 ** get_address_of_laserRenderer_15() { return &___laserRenderer_15; }
	inline void set_laserRenderer_15(LineRenderer_t849157671 * value)
	{
		___laserRenderer_15 = value;
		Il2CppCodeGenWriteBarrier(&___laserRenderer_15, value);
	}

	inline static int32_t get_offset_of_laserController_16() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___laserController_16)); }
	inline LaserController_t2836074815 * get_laserController_16() const { return ___laserController_16; }
	inline LaserController_t2836074815 ** get_address_of_laserController_16() { return &___laserController_16; }
	inline void set_laserController_16(LaserController_t2836074815 * value)
	{
		___laserController_16 = value;
		Il2CppCodeGenWriteBarrier(&___laserController_16, value);
	}

	inline static int32_t get_offset_of_instructions_17() { return static_cast<int32_t>(offsetof(HeroController_t4041626604, ___instructions_17)); }
	inline InstructionsController_t1323375089 * get_instructions_17() const { return ___instructions_17; }
	inline InstructionsController_t1323375089 ** get_address_of_instructions_17() { return &___instructions_17; }
	inline void set_instructions_17(InstructionsController_t1323375089 * value)
	{
		___instructions_17 = value;
		Il2CppCodeGenWriteBarrier(&___instructions_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
