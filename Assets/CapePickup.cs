﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapePickup : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag ("Player")) {
			HeroController hero = other.transform.parent.GetComponent<HeroController> ();
			hero.PickupCape ();

			Destroy (gameObject);
		}
	}
}
