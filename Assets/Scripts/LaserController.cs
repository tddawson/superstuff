﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : MonoBehaviour {


	public IEnumerator ShowLaser() {
		gameObject.SetActive (true);

		yield return new WaitForSeconds (.05f);
		HideLaser ();
	}

	public void HideLaser() {
		gameObject.SetActive (false);
	}
}
