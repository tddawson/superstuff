using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour {

	public float jumpPower;
	public GameObject body;
	public GameObject eye;
	public GameObject cape;


	// Hero's powers
	public float maxPower = 100;
	public float powerToJump = 5;
	public float powerToShoot = 15;
	// public float powerToInvisible = 25;
	
	private float powerRemaining;
	private bool hasFlight;
	private bool hasLaserVision;


	public RectTransform powerMeter;

	private bool isOnFloor;

	public GameObject laser;
	private LineRenderer laserRenderer;
	private LaserController laserController;

	public InstructionsController instructions;

	// Use this for initialization
	void Start () {
		powerRemaining = 0;
		laserController = laser.GetComponent<LaserController> ();
		laserRenderer = laser.GetComponent<LineRenderer> ();
		isOnFloor = false;

		hasFlight = PlayerPrefs.GetInt("requested_level", 1) > 1;
		hasLaserVision = PlayerPrefs.GetInt("requested_level", 1) > 1;
		Debug.Log(hasFlight);

		UpdatePowerMeter();
	}

	public void AttemptJump()
	{
		if (isOnFloor) {
			Jump ();
		}
		else if (hasFlight && powerRemaining >= powerToJump) {
			Jump ();
			powerRemaining -= powerToJump;
			UpdatePowerMeter ();
		}
		// TODO play failure sound if can't jump
	}

	void Jump()
	{
		Rigidbody2D rigidBody = body.GetComponent<Rigidbody2D>();
		Vector2 v = rigidBody.velocity;
		v.y = jumpPower;
		rigidBody.velocity = v;
		isOnFloor = false;
	}

	public void LandOnFloor()
	{
		isOnFloor = true;
	}

	public void AttemptShotAt(Vector2 targetPos) {
		if (hasLaserVision && powerRemaining >= powerToShoot) {
			ShootAt (targetPos);
		}
		// TODO play failure sound if can't shoot
	}

	void ShootAt(Vector2 targetPos) {
		Vector2 origin = eye.transform.position;
		Vector2 dir = targetPos - origin;
		float dist = dir.magnitude;
		Debug.Log(dist);
		RaycastHit2D hit = Physics2D.Raycast (origin, dir, dist);
		if (hit && hit.collider) {
			if (hit.collider.CompareTag ("obstacle")) {
				Destroy (hit.collider.gameObject);
				// TODO shower with particle effects
			}
			else if (hit.collider.CompareTag ("obstacle_top")) {
				Destroy (hit.collider.transform.parent.gameObject);
				// TODO shower with particle effects
			}
		}

		// TODO draw the ray
		laserRenderer.SetPosition(0, origin);
		laserRenderer.SetPosition(1, targetPos);
		StartCoroutine(laserController.ShowLaser());

		powerRemaining -= powerToShoot;
		UpdatePowerMeter ();
	}

	public void ReplenishPower()
	{
		powerRemaining = Mathf.Min(powerRemaining + 50, maxPower);
		UpdatePowerMeter ();

		bool firstPowerup = PlayerPrefs.GetInt("num_powerups", 0) == 1;
		if (firstPowerup && LevelGenerator.level == 1)
		{
			instructions.ShowPowerInstructions();
		}
	}

	public void PickupCape()
	{
		hasFlight = true;
		instructions.ShowCapeInstructions();
		cape.SetActive(true);
	}

	public void PickupEye()
	{
		hasLaserVision = true;
		instructions.ShowEyeInstructions();
		eye.SetActive(true);
	}

	void UpdatePowerMeter() 
	{
		float percentage = 100f * powerRemaining / maxPower;
		powerMeter.sizeDelta = new Vector2(percentage, 15);
	}
}
