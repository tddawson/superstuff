﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag ("Player")) {
			int numPowerups = PlayerPrefs.GetInt("num_powerups", 0);
			PlayerPrefs.SetInt("num_powerups", numPowerups + 1);

			HeroController hero = other.transform.parent.GetComponent<HeroController> ();
			hero.ReplenishPower ();

			Destroy (gameObject);
		}
	}
}
