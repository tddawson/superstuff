﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public bool isMobile;
	public GameObject heroContainer;
	HeroController heroController;

	// Use this for initialization
	void Start () {
		heroController = heroContainer.GetComponent<HeroController> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (isMobile) {
			for (int i = 0; i < Input.touchCount; i++) {
				Touch touch = Input.GetTouch (i);
				if (touch.phase == TouchPhase.Began) {
					HandleTouchAt (touch.position);
				}
			}
		} else {
			if (Input.GetMouseButtonDown (0)) {
				HandleTouchAt (Input.mousePosition);
			}
		}
		if (Input.GetKeyDown ("up") || Input.GetKeyDown("w")) {
			heroController.AttemptJump ();
		}
	}

	void HandleTouchAt(Vector2 pos)
	{
		// On mobile, use left part of screen for jumping, right for lasering
		if (isMobile)
		{
			if (pos.x > Screen.width / 5) {
				heroController.AttemptShotAt (WorldPoint (pos));
			} else {
				heroController.AttemptJump ();
			}	
		}
		else
		{
			// On computer, any click is a shot
			heroController.AttemptShotAt (WorldPoint (pos));
		}
		
	}

	Vector2 WorldPoint(Vector2 pos) {
		Vector3 pos3 = new Vector3 (pos.x, pos.y, 0);
		Vector3 worldPos = Camera.main.ScreenToWorldPoint (pos3);
		return new Vector2 (worldPos.x, worldPos.y);
	}


}
