﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

	Vector2 movement = new Vector2 (6f, 0f);

	// Update is called once per frame
	void Update () {
		Vector2 frameMovement = Time.deltaTime * movement;
		transform.localPosition = (Vector2) transform.localPosition - frameMovement;
	}
}
