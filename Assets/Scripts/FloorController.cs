﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorController : MonoBehaviour {

	public void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.CompareTag ("Player")) {
			HeroController heroController = GameObject.Find("Hero").GetComponent<HeroController>();
			heroController.LandOnFloor ();
		}
	}
}
