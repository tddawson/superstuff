﻿using UnityEngine;

public class LevelGenerator : MonoBehaviour {

	public GameObject eye;
	public GameObject cape;

	public Texture2D[] maps;
	public GameObject obstaclePrefab;
	public GameObject powerupPrefab;
	public GameObject collectablePrefab;
	public GameObject finishPrefab;
	public GameObject capePrefab;
	public GameObject eyePrefab;

	public InstructionsController instructions;

	private Color obstacleColor;
	private Color powerupColor;
	private Color collectableColor;
	private Color finishColor;
	private Color eyeColor;
	private Color capeColor;


	public static int level;

	// Use this for initialization
	void Start () {
		obstacleColor = new Color(1, 0, 0); // Pure red
		powerupColor = new Color (1, 1, 0); // Pure yellow
		collectableColor = new Color(1, 0, 1); // Magenta
		finishColor = new Color(0, 1, 0); // Pure green

		// Only on tutorial level
		capeColor = new Color(0, 0, 1); // Pure blue
		eyeColor = new Color(0, 1, 1); // Pure cyan

		LevelGenerator.level = PlayerPrefs.GetInt("requested_level", 1);
		int levelIndex = LevelGenerator.level - 1;
		GenerateLevel (levelIndex);

		// Tutorial. Start off with some instructions
		if (levelIndex == 0)
		{
			bool justDied = PlayerPrefs.GetInt("just_died", 0) == 1;
			if (justDied)
			{
				instructions.ShowWallInstructions();
			}
			else {
				instructions.ShowJumpInstructions();
			}

			// Don't start with the cape or the eye on tutorial level
			eye.SetActive(false);
			cape.SetActive(false);
		}
		PlayerPrefs.SetInt("just_died", 0);
		PlayerPrefs.SetInt("num_powerups", 0);
	}

	void GenerateLevel(int level) {
		Texture2D map = maps[level];

		for (int x = 0; x < map.width; x++) {
			for (int y = 0; y < map.height; y++) {
				Color pixelColor = map.GetPixel(x, y);
				if (pixelColor.a == 0)
					continue;


				GenerateTile (x, y, pixelColor);
			}
		}
	}

	void GenerateTile(int x, int y, Color pixelColor) {
		float worldY = y - 3.5f;
		float worldX = 2 * x;
		Vector2 position = new Vector2 (worldX, worldY);

		if (pixelColor.Equals(obstacleColor))
		{
			Instantiate (obstaclePrefab, position, Quaternion.identity, transform);
		}
		if (pixelColor.Equals(powerupColor))
		{
			Instantiate (powerupPrefab, position, powerupPrefab.transform.rotation, transform);
		}
		if (pixelColor.Equals(collectableColor))
		{
			// TODO Implement collectables
			// Instantiate (collectablePrefab, position, collectablePrefab.transform.rotation, transform);	
		}
		if (pixelColor.Equals(finishColor))
		{
			position.y = -4f;
			Instantiate (finishPrefab, position, finishPrefab.transform.rotation, transform);	
		}
		if (pixelColor.Equals(eyeColor))
		{
			Instantiate (eyePrefab, position, eyePrefab.transform.rotation, transform);	
		}
		if (pixelColor.Equals(capeColor))
		{
			Instantiate (capePrefab, position, capePrefab.transform.rotation, transform);	
		}

	}
	
}
