﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObstacleController : MonoBehaviour {

	public void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.CompareTag ("Player")) {
			// TODO display failure
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			PlayerPrefs.SetInt("just_died", 1);
		}
	}

}
