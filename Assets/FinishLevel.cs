﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLevel : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag ("Player")) {
			if (LevelGenerator.level > PlayerPrefs.GetInt("furthest_level_completed", 0))
			{
				PlayerPrefs.SetInt("furthest_level_completed", LevelGenerator.level);
			}

			// Go back to home menu
			SceneManager.LoadScene("Menu");
		}
	}
}
