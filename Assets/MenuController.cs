﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	public GameObject btn1;
	public GameObject btn2;
	public GameObject btn3;
	public GameObject btn4;
	public GameObject btn5;

	private Color completeColor;
	private Color nextColor;
	private Color futureColor;

	// Use this for initialization
	void Start () {
		ColorUtility.TryParseHtmlString("#6FCF97", out completeColor);
		ColorUtility.TryParseHtmlString("#FFE474", out nextColor);
		ColorUtility.TryParseHtmlString("#C4C4C4", out futureColor);

		int nextLevel = PlayerPrefs.GetInt("furthest_level_completed", 0) + 1;


		if (nextLevel == 1)
		{
			btn1.GetComponent<Image>().color = nextColor;
		}
		else {
			btn1.GetComponent<Image>().color = completeColor;
			if (nextLevel == 2)
			{
				btn2.GetComponent<Image>().color = nextColor;
			}
			else
			{
				btn2.GetComponent<Image>().color = completeColor;
				if (nextLevel == 3)
				{
					btn3.GetComponent<Image>().color = nextColor;
				}
				else
				{
					btn3.GetComponent<Image>().color = completeColor;
					if (nextLevel == 4)
					{
						btn4.GetComponent<Image>().color = nextColor;
					}
					else
					{
						btn4.GetComponent<Image>().color = completeColor;
						btn5.GetComponent<Image>().color = nextLevel == 5 ? nextColor : completeColor;
					}
				}
			}
		}


		// if 
		// GetComponent<Image>().color = Color.red;
	}
	
	public void StartLevel(int level)
	{
		if (level > PlayerPrefs.GetInt("furthest_level_completed", 0) + 1)
		{
			Debug.Log("Can't play level " + level);
		}
		else {
			PlayerPrefs.SetInt("requested_level", level);
			SceneManager.LoadScene("Game");	
		}

	}
}
