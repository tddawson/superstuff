﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionsController : MonoBehaviour {

	public GameObject jumpInstructions;
	public GameObject powerInstructions;
	public GameObject capeInstructions;
	public GameObject eyeInstructions;
	public GameObject wallInstructions;

	private bool timerRunning = false;
	private float timer = 4f;

	// Use this for initialization
	void Start () {
		HideAll();
	}

	void Update() {
		if (timerRunning)
		{
			timer -= Time.deltaTime;
			if (timer <= 0)
			{
				HideAll();
				timerRunning = false;
			}	
		}
	}

	void StartTimer() {
		timer = 4f;
		timerRunning = true;
	}
	
	void HideAll() {
		jumpInstructions.SetActive(false);
		wallInstructions.SetActive(false);
		eyeInstructions.SetActive(false);
		capeInstructions.SetActive(false);
		powerInstructions.SetActive(false);
	}

	public void ShowJumpInstructions()
	{
		HideAll();
		jumpInstructions.SetActive(true);
		StartTimer();
	}

	public void ShowWallInstructions()
	{
		HideAll();
		wallInstructions.SetActive(true);
		StartTimer();
	}

	public void ShowCapeInstructions()
	{
		HideAll();
		capeInstructions.SetActive(true);
		StartTimer();
	}

	public void ShowEyeInstructions()
	{
		HideAll();
		eyeInstructions.SetActive(true);
		StartTimer();
	}

	public void ShowPowerInstructions()
	{
		HideAll();
		powerInstructions.SetActive(true);
		StartTimer();
	}
}
